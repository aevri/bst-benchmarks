from csv import DictReader

import click
import pandas as pd
from tabulate import tabulate


def show_report(report_file):
    df = pd.read_csv(report_file)

    df = (
        df.groupby(["commit", "action", "python_version"])
        .median()
        .reset_index()
    )
    df["max_memory"] /= 1024
    df["max_memory"] = df["max_memory"].round()

    values = df.values.tolist()

    last_step = None
    for value in values:
        if value[0] == last_step:
            value[0] = ""
        else:
            last_step = value[0]

    print("-" * 80)
    print(tabulate(values, headers=df.columns.values, tablefmt="pipe"))
    print("-" * 80)


@click.command()
@click.argument("report_file", type=click.Path("r"))
def report(report_file):
    show_report(report_file)


if __name__ == "__main__":
    report()
