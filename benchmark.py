#!/usr/bin/env python3

from contextlib import suppress
from csv import DictWriter
import functools
import json
import os
from pathlib import Path
import shutil
import subprocess
import sys
from typing import Callable, Dict, List, Optional, Tuple, Union

import click
from tqdm import tqdm, trange

import report


ROOT_PATH = Path(__file__).resolve().parent
CONFIGURATIONS_PATH = ROOT_PATH.joinpath("configs")
CACHE_PATH = ROOT_PATH.joinpath("cache")
TOX_WORKDIR_PATH = CACHE_PATH.joinpath("tox")
BUILDSTREAM_CACHE = CACHE_PATH.joinpath("bst-cache")

BUILDSTREAM_PATH = Path(os.getenv(
    "BST_REPOSITORY", CACHE_PATH.joinpath("buildstream")
))
BUILDSTREAM_REMOTE = "https://gitlab.com/buildstream/buildstream.git"

PROJECT_PATH = CACHE_PATH.joinpath("project")
PROJECT_REMOTE = "https://gitlab.com/jennis/debian-stretch-bst.git"
PROJECT_BRANCH = "origin/jennis/add_all_files"


class Configuration:
    @classmethod
    def list(cls) -> List[str]:
        return [path.stem for path in CONFIGURATIONS_PATH.glob("*")]

    def __init__(self, config_name: str) -> None:
        with CONFIGURATIONS_PATH.joinpath(config_name+".json").open() as fp:
            data = json.load(fp)

        self.builders = data["builders"]
        self.files = data["files"]
        self.runs = data["runs"]
        self.warmups = data["warmups"]

        self.python_versions = [
            version
            if version != "current"
            else "py{}{}".format(sys.version_info.major, sys.version_info.minor)
            for version in data["python-versions"]
        ]


class Action:
    def cleanup(self) -> None:
        pass

    def get_command(self, bst: str, elements: List[str]) -> List[str]:
        raise NotImplementedError()

    def __str__(self):
        raise NotImplementedError()


class ShowAction(Action):
    def __init__(self, type_: Optional[str] = None):
        self.type = type_

    def get_command(self, bst: str, elements: List[str]) -> List[str]:
        return [bst, "show", *elements]

    def __str__(self):
        if self.type:
            return "show - {}".format(self.type)
        return "show"


class BuildAction(Action):
    def __init__(self, n_builders: int) -> None:
        self.n_builders = n_builders

    def cleanup(self):
        with suppress(FileNotFoundError):
            shutil.rmtree(str(BUILDSTREAM_CACHE))

    def get_command(self, bst, elements):
        return [bst, "--builders", str(self.n_builders), "build", *elements]

    def __str__(self):
        return "build - {}".format(self.n_builders)


def run(cmd, cwd: Optional[Path] = None, env: Optional[Dict[str, str]] = None):
    try:
        return subprocess.check_output(
            cmd,
            cwd=None if cwd is None else str(cwd),
            stderr=subprocess.STDOUT,
            env=env,
            universal_newlines=True,
        )
    except subprocess.CalledProcessError as exc:
        print(exc.stdout, file=sys.stderr)
        raise


def time_command(command: List[str], cwd: Union[Path, str]) -> Tuple[str, str]:
    environ = os.environ.copy()
    environ["XDG_CACHE_HOME"] = str(BUILDSTREAM_CACHE)

    output = (
        run(
            ["/usr/bin/time", "--format", "%e %M", "--", *command],
            cwd=cwd,
            env=environ,
        )
        .strip()
        .splitlines()
    )

    return output[-1].split(" ")


def update_buildstream_mirror():
    if not BUILDSTREAM_PATH.exists():
        run(["git", "clone", "--mirror", BUILDSTREAM_REMOTE, str(BUILDSTREAM_PATH)])
    else:
        run(["git", "remote", "update"], cwd=BUILDSTREAM_PATH)


def update_project():
    if not PROJECT_PATH.exists():
        run(["git", "clone", PROJECT_REMOTE, str(PROJECT_PATH)])
    else:
        run(["git", "fetch"], cwd=PROJECT_PATH)

    run(["git", "reset", "--hard", PROJECT_BRANCH], cwd=str(PROJECT_PATH))
    run(["./prepare_project.sh"], cwd=PROJECT_PATH)


def get_commits_between(git_repo: Path, base: str, head: str) -> List[str]:
    all_commits = (
        run(["git", "rev-list", "{}..{}".format(base, head)], cwd=git_repo)
        .strip()
        .splitlines()
    )
    commits = [head]
    commits.extend(all_commits[1:-1])
    commits.append(base)
    return commits


def get_commit_sha(commit: str):
    return run(
        ["git", "rev-parse", "--short", "--verify", commit],
        cwd=BUILDSTREAM_PATH,
    ).strip()


def create_tox_environment(python_version: str, commit: str):
    environ = os.environ.copy()
    environ["BST_COMMIT"] = commit
    environ["BST_REPOSITORY"] = str(BUILDSTREAM_PATH)

    run(
        [
            "tox",
            "--notest",
            "--recreate",
            "--workdir",
            str(TOX_WORKDIR_PATH),
            "-e",
            python_version,
        ],
        env=environ,
    )


def benchmark(
    configuration: Configuration,
    get_commits: Callable[[Union[str, Path]], List[str]],
    show_only: bool,
):
    update_buildstream_mirror()
    update_project()

    commits = get_commits(BUILDSTREAM_PATH)
    commit_shas = [get_commit_sha(commit) for commit in commits]

    with suppress(FileNotFoundError):
        shutil.rmtree(str(BUILDSTREAM_CACHE))

    actions = [ShowAction()] # type: List[Action]

    if not show_only:
        for builders in configuration.builders:
            actions.append(BuildAction(builders))

        actions.append(ShowAction("cached"))

    runs = configuration.runs + configuration.warmups

    timings = []

    tqdm_commits = tqdm(commit_shas)

    for commit_ref, commit_sha in zip(commits, tqdm_commits):
        if commit_ref.startswith(commit_sha):
            commit = commit_ref
        else:
            commit = "{} - {}".format(commit_ref, commit_sha)

        tqdm_commits.set_description("commit '{}'".format(commit))

        tqdm_python_version = tqdm(configuration.python_versions, leave=False)
        for python_version in tqdm_python_version:
            tqdm_python_version.set_description("python '{}'".format(python_version))

            create_tox_environment(python_version, commit_sha)

            bst_command = TOX_WORKDIR_PATH.joinpath(
                python_version, "bin", "bst"
            )
            tqdm_actions = tqdm(actions, leave=False)
            for action in tqdm_actions:
                tqdm_actions.set_description("action '{}'".format(action))

                for iteration in trange(runs, desc="iteration", leave=False):

                    action.cleanup()

                    timing = time_command(
                        action.get_command(str(bst_command), configuration.files),
                        cwd=PROJECT_PATH,
                    )

                    if iteration >= configuration.warmups:
                        timings.append(
                            {
                                "commit": commit,
                                "python_version": python_version,
                                "action": str(action),
                                "max_memory": timing[1],
                                "time": timing[0],
                            }
                        )

    return timings


@click.command()
@click.option(
    "--config", type=click.Choice(Configuration.list()), default="quick"
)
@click.option(
    "--report-path",
    type=click.Path(file_okay=True, writable=True),
    default=os.path.join(os.getcwd(), "report.csv"),
)
@click.option("--between", is_flag=True)
@click.option("--show-only", is_flag=True)
@click.argument("reference")
@click.argument("candidate")
def compare(
    config: str,
    reference: str,
    candidate: str,
    report_path: str,
    between: bool,
    show_only: bool,
):
    if between:
        get_commits = functools.partial(
            get_commits_between, base=reference, head=candidate
        )
    else:
        get_commits = lambda _: [reference, candidate]

    results = benchmark(Configuration(config), get_commits, show_only=show_only)

    with open(report_path, "w") as fp:
        writer = DictWriter(fp, results[0].keys())

        writer.writeheader()
        writer.writerows(results)

    report.show_report(report_path)


if __name__ == "__main__":
    compare()
